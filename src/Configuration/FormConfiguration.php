<?php

namespace GetRepo\FormYaml\Configuration;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvents;

class FormConfiguration implements ConfigurationInterface
{
    /** @var string */
    final public const NAME = 'form_definitions';
    /** @var string */
    final public const DEFAULT_TYPE = TextType::class;

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::NAME);
        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        // @phpstan-ignore-next-line
        $rootNode
            ->useAttributeAsKey('name')
            ->arrayPrototype()
                ->canBeDisabled()
                ->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('type')
                        ->cannotBeEmpty()
                        ->defaultValue(self::DEFAULT_TYPE)
                    ->end()
                    ->arrayNode('options')
                        ->variablePrototype()->end()
                    ->end()
                    ->scalarNode('condition')
                        ->defaultNull()
                    ->end()
                    ->arrayNode('events')
                        ->children()
                            ->append($this->getFormEventNode(FormEvents::PRE_SUBMIT))
                            ->append($this->getFormEventNode(FormEvents::SUBMIT))
                            ->append($this->getFormEventNode(FormEvents::POST_SUBMIT))
                            ->append($this->getFormEventNode(FormEvents::PRE_SET_DATA))
                            ->append($this->getFormEventNode(FormEvents::POST_SET_DATA))
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }

    private function getFormEventNode(string $name): ArrayNodeDefinition
    {
        $treeBuilder = new TreeBuilder(
            \str_replace(['.', 'form_'], ['_', ''], $name)
        );
        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        // @phpstan-ignore-next-line
        return $rootNode
            ->beforeNormalization()
                ->ifString()
                ->then(fn ($callback): array => ['callback' => $callback])
            ->end()
            ->children()
                ->variableNode('callback')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->integerNode('priority')->end()
            ->end()
        ;
    }
}
