<?php

namespace GetRepo\FormYaml;

use Doctrine\Inflector\InflectorFactory;
use GetRepo\FormYaml\Configuration\FormConfiguration;
use GetRepo\FormYaml\Resolver\Resolver;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Yaml\Yaml;

class ConfigurableEntityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Resolver $resolver */
        $resolver = $options['resolver'];
        $entity = $options['data'];

        // Expression language for condition filter
        $expressionLanguage = new ExpressionLanguage();

        $fields = (new Processor())->processConfiguration(new FormConfiguration(), [$options[FormConfiguration::NAME]]);
        $i = count($fields);
        foreach ($fields as $formName => $formConf) {
            if (!$formConf['enabled']) {
                continue;
            }
            // apply condition
            if ($formConf['condition'] ?? false) {
                $res = (bool) $expressionLanguage->evaluate(
                    $formConf['condition'],
                    \array_merge(
                        [
                            'form_name' => $formName,
                            'form_options' => $options,
                        ],
                        $options['expression_language_values']
                    )
                );
                if (!$res) {
                    continue;
                }
            }

            // priority ordering counter
            $i--;

            // create form
            $formType = $resolver->resolveFormClass($formConf['type']);
            $formOptions = $resolver->resolveFormOptions($formType, $formConf['options'] ?? []);
            if (!array_key_exists('priority', $formOptions)) {
                $formOptions['priority'] = $i;
            }
            $builder->add($formName, $formType, $formOptions);
            $inflector = InflectorFactory::create()->build();

            // data transformer
            $nameMethod = ucfirst($inflector->camelize($formName));
            $transform = [$entity, "transformData{$nameMethod}"];
            $reverseTransform = [$entity, "reverseTransformData{$nameMethod}"];
            if (is_callable($transform) && is_callable($reverseTransform)) {
                $builder->get($formName)->addModelTransformer(
                    new CallbackTransformer($transform, $reverseTransform)
                );
            }

            // optional events
            if ($formConf['events'] ?? false) {
                foreach ($formConf['events'] as $eventName => $eventConf) {
                    if (is_string($eventConf['callback']) && is_callable([$entity, $eventConf['callback']])) {
                        $callback = $resolver->resolveCallBack([$entity, $eventConf['callback']]);
                    } else {
                        $callback = $resolver->resolveCallBack($eventConf['callback']);
                    }
                    if (!\is_callable($callback)) {
                        $message = \sprintf(
                            "Callback for form event '%s' was not found.\n - Field: %s :: %s\n - Callback: %s.",
                            $eventName,
                            $entity::class,
                            $formName,
                            \var_export($eventConf['callback'], true)
                        );
                        throw new InvalidConfigurationException($message);
                    }

                    $builder->get($formName)
                        ->addEventListener(
                            constant(FormEvents::class . '::' . strtoupper((string) $eventName)),
                            $callback,
                            ($eventConf['priority'] ?? 0)
                        );
                }
            }
        }

        // submit button
        if ($options['submit']) {
            $builder->add(
                '_submit',
                SubmitType::class,
                [
                    'label' => $options['submit_label'],
                    'attr' => $options['submit_attr'],
                ]
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        // Resolver option
        $resolver->setDefault('resolver', new Resolver());
        $resolver->addAllowedTypes('resolver', Resolver::class);

        // data entity object
        $resolver->setRequired('data');
        $resolver->addAllowedTypes('data', ['object', 'string']);
        $resolver->setNormalizer('data', function (OptionsResolver $options, $data) {
            if (\is_string($data)) {
                if (!\class_exists($data)) {
                    throw new UndefinedOptionsException("Data class '{$data}' does not exists.");
                }
                $data = (new \ReflectionClass($data))->newInstanceWithoutConstructor();
            }

            return $data;
        });

        // form field config
        $resolver->setRequired(FormConfiguration::NAME);
        $resolver->addAllowedTypes(FormConfiguration::NAME, ['array', 'string']);
        $resolver->setNormalizer(FormConfiguration::NAME, function (OptionsResolver $options, $value): array {
            // handle file path
            if (is_string($value)) {
                $value = (array) Yaml::parseFile($value);
            }

            return $value;
        });

        // Expression language values
        $resolver->setDefault('expression_language_values', []);
        $resolver->addAllowedTypes('expression_language_values', 'array');

        // Submit button options
        $resolver->setDefault('submit', true);
        $resolver->addAllowedTypes('submit', 'boolean');
        $resolver->setDefault('submit_label', 'Submit');
        $resolver->addAllowedTypes('submit_label', ['string', 'null']);
        $resolver->setDefault('submit_attr', []);
        $resolver->addAllowedTypes('submit_attr', 'array');
    }
}
