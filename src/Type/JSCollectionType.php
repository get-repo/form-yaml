<?php

namespace GetRepo\FormYaml\Type;

use Doctrine\Inflector\Inflector;
use Doctrine\Inflector\InflectorFactory;
use GetRepo\FormYaml\Resolver\Resolver;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\Event\PostSubmitEvent;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormRendererInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

class JSCollectionType extends CollectionType
{
    private readonly Inflector $inflector;

    public function __construct(
        #[Autowire(service: 'twig.form.renderer')]
        private readonly FormRendererInterface $formRenderer,
        private readonly Resolver $resolver,
    ) {
        $this->inflector = InflectorFactory::create()->build();
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        // Try to set the value of each element of the collection back to the entity
        if ($mappedPath = $options['mapped_path']) {
            $builder->addEventListener(
                FormEvents::POST_SUBMIT,
                function (PostSubmitEvent $event) use ($mappedPath): void {
                    $form = $event->getForm();
                    $entity = $form->getParent()->getData();
                    $pa = PropertyAccess::createPropertyAccessorBuilder()
                        ->enableExceptionOnInvalidIndex()
                        ->enableExceptionOnInvalidPropertyPath()
                        ->disableMagicCall()
                        ->getPropertyAccessor();

                    foreach ($event->getData() as $element) {
                        $pa->setValue($element, $mappedPath, $entity);
                    }
                }
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        // Method to call to set the referenced value of each element of the collection
        $resolver->setDefault('mapped_path', null);
        $resolver->setAllowedTypes('mapped_path', ['null', 'string']);

        // icons options
        $iconOptionsMap = [
            'icon_add_class' => null,
            'icon_add_text' => 'Add',
            'icon_delete_class' => null,
            'icon_delete_text' => 'Remove',
        ];
        foreach ($iconOptionsMap as $name => $defaultValue) {
            $resolver->setDefault($name, $defaultValue);
            $resolver->setAllowedTypes($name, ['null', 'string']);
        }

        // classes options
        $resolver->setDefault('container_class', null);

        // override entry_options
        $resolver->setNormalizer('entry_options', function (OptionsResolver $resolver, $entryOptions) {
            // force no label
            $entryOptions['label'] = false;
            // add delete button for exsting elements
            if ($resolver['allow_delete']) {
                $entryOptions['attr']['onload'] = str_replace("\n", ' ', "javascript:
                    span = document.createElement('span');
                    span.setAttribute('id', 'delete_' + this.id);
                    span.setAttribute('class', '{$resolver['icon_delete_class']}');
                    span.innerText = '{$resolver['icon_delete_text']}';
                    span.style.cursor = 'pointer';
                    span.setAttribute('onclick', 'javascript: this.parentElement.remove();');
                    this.parentElement.prepend(span);
                ");
            }

            // resolve constraints
            if ($entryOptions['constraints'] ?? false) {
                $entryOptions['constraints'] = $this->resolver->resolveConstraints($entryOptions['constraints']);
            }

            $entryOptions['row_attr']['class'] = trim(sprintf(
                '%s js-collection-element',
                $entryOptions['row_attr']['class'] ?? '',
            ));

            return $entryOptions;
        });
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        parent::buildView($view, $form, $options);

        $onload = '';
        if ($options['allow_add']) {
            $id = $view->vars['id'];
            $singular = $this->inflector->singularize($view->vars['name']);

            $prototype = $this->formRenderer->renderBlock($view->vars['prototype'], 'form_row');

            // handle new element prototype
            $onclick = "javascript:
                window.jsCollectionElementsCounter++;
                container = document.getElementById('{$id}');
                count = container.childElementCount;
                div = document.createElement('div');
                div.innerHTML = \"" . str_replace('"', '\\\\\\"', $prototype) . "\"
                    .trim()
                    .replace(/__name__label__/g, '')
                    .replace(/__name__/g, window.jsCollectionElementsCounter);
                newnode = div.firstChild;
                container.appendChild(newnode);
            ";

            // add delete button for new elements
            if ($options['allow_delete']) {
                $onclick .= "
                    span = document.createElement('span');
                    span.setAttribute('id', 'delete_{$id}_' + window.jsCollectionElementsCounter);
                    span.setAttribute('class', '{$options['icon_delete_class']}');
                    span.innerText = '{$options['icon_delete_text']}';
                    span.style.cursor = 'pointer';
                    span.setAttribute('onclick', 'javascript: this.parentElement.remove();');
                    newnode.prepend(span);
                ";
            }

            // add new element icon
            $onclick = str_replace("'", "\'", $onclick);
            $onload .= preg_replace(["/(\n+|\s{2,})/", "/;\s+/"], [' ', ';'], "javascript:
                window.jsCollectionElementsCounter = document.querySelectorAll('.js-collection-element').length - 1;
                span = document.createElement('span');
                span.setAttribute('id', 'add_new_{$singular}');
                span.setAttribute('class', '{$options['icon_add_class']}');
                span.innerText = '{$options['icon_add_text']}';
                span.style.cursor = 'pointer';
                span.setAttribute('onclick', '{$onclick}');
                this.parentElement.prepend(span);
            ");
        }

        if ($onload) {
            $view->vars['attr']['onload'] = $onload;
        }

        $view->vars['attr']['class'] = $options['container_class'];
    }

    public function getBlockPrefix(): string
    {
        return 'js_collection';
    }
}
