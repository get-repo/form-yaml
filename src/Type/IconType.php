<?php

namespace GetRepo\FormYaml\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class IconType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        parent::buildView($view, $form, $options);

        $id = $view->vars['id'];
        $onload = <<<JAVASCRIPT
[].forEach.call(document.querySelectorAll('img.{$id}-icon'), function(el) {
    el.remove();
});
label = document.querySelector('label[for="{$id}"]');
if (label) {
    label.insertAdjacentHTML(
        'afterend',
        '<img src="' + this.value + '"
              class="{$id}-icon"
              style="padding-left: 10px; height: 20px; width: auto;"/>'
    );
}
JAVASCRIPT;

        $view->vars['attr']['onload'] = $view->vars['attr']['onkeyup'] = preg_replace(
            ["/(\n+|\s{2,})/", "/;\s+/"],
            [' ', ';'],
            "javascript: {$onload}"
        );
    }

    public function getParent(): string
    {
        return UrlType::class;
    }
}
