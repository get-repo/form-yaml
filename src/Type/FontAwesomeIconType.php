<?php

namespace GetRepo\FormYaml\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class FontAwesomeIconType extends AbstractType
{
    final public const ICON_CLASS_REGEX = '/^fa[a-z] fa-[a-z\-]+$/';

    /**
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
           'attr' => ['placeholder' => 'Font Awesome icon class (ex: fas fa-won-sign)'],
           'help_html' => true,
           'help' => <<<HELP
preview:
<span class="preview" style="margin-right: 3%;"></span>
<span
 onclick="window.open('https://fontawesome.com/v5/search', '_blank').focus();"
 style="cursor: pointer; text-decoration: underline;">
    https://fontawesome.com/v5/search
</span>
HELP,
            'constraints' => [
                new Regex([
                    'pattern' => self::ICON_CLASS_REGEX,
                    'message' => 'This is not a valid Font Awesome icon class (ex: fas fa-won-sign)',
                ]),
            ],
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        parent::buildView($view, $form, $options);

        $onload = <<<JAVASCRIPT
pEl = this.parentNode.querySelector(".preview");
pEl.innerHTML = '<i class="'+this.value+'"></i>';
JAVASCRIPT;

        $view->vars['attr']['onload'] = $view->vars['attr']['onkeyup'] = preg_replace(
            ["/(\n+|\s{2,})/", "/;\s+/"],
            [' ', ';'],
            "javascript: {$onload}"
        );
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
