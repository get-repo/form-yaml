<?php

namespace GetRepo\FormYaml\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/** @see https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-text-security */
class ProtectedTextType extends AbstractType
{
    public const STYLE_DISC = 'disc';
    public const STYLE_CIRCLE = 'circle';
    public const STYLE_SQUARE = 'square';

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $style = '';
        if (array_key_exists('style', $view->vars['attr'])) {
            // cleaning spaces and chars ";", and making sure it ends with "; "
            $style = trim($view->vars['attr']['style'], ' ;') . '; ';
        }

        $view->vars['attr']['style'] = trim(sprintf(
            '%s%s',
            $style,
            str_replace(PHP_EOL, ' ', <<<STYLE
text-security: {$options['security_style']};
-webkit-text-security: {$options['security_style']};
-moz-text-security: {$options['security_style']};
STYLE),
        ));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('security_style', self::STYLE_DISC);
        $resolver->setAllowedValues('security_style', [self::STYLE_DISC, self::STYLE_CIRCLE, self::STYLE_SQUARE]);
    }

    public function getParent(): ?string
    {
        return TextType::class;
    }
}
