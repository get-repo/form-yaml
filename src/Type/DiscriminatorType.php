<?php

namespace GetRepo\FormYaml\Type;

use Doctrine\ORM\EntityManagerInterface;
use GetRepo\FormYaml\Resolver\Resolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class DiscriminatorType extends AbstractType
{
    public const KEY = 'discriminator';

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Resolver $resolver,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $discriminatorClass = $options['discriminator_class'];

        $metadata = $this->entityManager->getClassMetadata($discriminatorClass);
        if (!$metadata->discriminatorMap) {
            $message = sprintf(
                'DiscriminatorType: The class \'%s\' does not have a discriminator.',
                $discriminatorClass
            );
            throw new InvalidConfigurationException($message);
        }
        $choices = [];
        foreach ($metadata->discriminatorMap as $name => $className) {
            $choices[$this->getShortName($className)] = $name;
        }

        // select box with discriminator map choices
        $builder->add(
            self::KEY,
            ChoiceType::class,
            [
                'choices' => $choices,
                'placeholder' => 'Choose an option',
                'required' => true,
                'label' => false,
                'attr' => [
                    'onload' => $this->getOnLoadScript(),
                    'onchange' => $this->getOnChangeScript(),
                ],
                'constraints' => [
                    new Assert\NotNull([
                        'message' => 'Please select an option',
                    ]),
                ],
            ]
        )->addModelTransformer(new CallbackTransformer(
            fn ($value) => $value,
            function ($value) {
                if (is_array($value)) {
                    if ($value[self::KEY] ?? false) {
                        return $value[$value[self::KEY]];
                    }
                    unset($value[self::KEY]);
                    $value = current($value);
                }

                return $value;
            }
        ));

        // unvisible discriminator forms
        /** @var string $child */
        foreach ($metadata->discriminatorMap as $child => $entity) {
            $resolved = $this->resolver->resolveFormClass(
                $this->getShortName($entity),
                ['default' => $options['entity_form'], 'disable_symfony_namespace' => true],
            );
            $builder->add(
                $child,
                $resolved,
                [
                    'data' => $entity,
                    'submit' => false,
                    'label' => false,
                    'attr' => ['data-discriminator-name' => $child, 'style' => 'display: none;'],
                    'constraints' => new Assert\Valid(),
                ]
            );
        }

        // select the right form when a value is already selected (for edit action or form errors)
        // NOTE: So far you can NOT change discriminator on edit.
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($metadata, $options): void {
                $form = $event->getForm();
                $data = $event->getData();
                if (\is_object($data)) {
                    // edit action does not need the discriminator selectbox
                    if ($form->has(self::KEY)) {
                        $form->remove(self::KEY);
                    }
                    /** @var string $child */
                    foreach ($metadata->discriminatorMap as $child => $entity) {
                        // remove all possible choices
                        if ($form->has($child)) {
                            $form->remove($child);
                        }
                        // and re-add the selected one to set the data option
                        if ($entity === $data::class) {
                            $resolved = $this->resolver->resolveFormClass(
                                $this->getShortName($entity),
                                ['default' => $options['entity_form'], 'disable_symfony_namespace' => true],
                            );
                            $form->add(
                                $child,
                                $resolved ?? $options['entity_form'],
                                [
                                    'mapped' => false,
                                    'data' => $data,
                                    'submit' => false,
                                    'label' => false,
                                    'constraints' => new Assert\Valid(),
                                ]
                            );
                        }
                    }
                }
            }
        );

        // remove the unselected forms on submit to avoid useless validation on them
        $builder->addEventListener(
            FormEvents::SUBMIT,
            function (FormEvent $event) use ($metadata): void {
                $form = $event->getForm();
                $data = $event->getData();

                if (
                    is_array($data)
                    && isset($data[self::KEY])
                    && $data[self::KEY]
                    && $form->has($data[self::KEY])
                ) {
                    $map = $metadata->discriminatorMap;
                    unset($map[$data[self::KEY]]);
                    /** @var string $child */
                    foreach (array_keys($map) as $child) {
                        if ($form->has($child)) {
                            $form->remove($child);
                        }
                    }
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setRequired('entity_form');
        $resolver->setAllowedTypes('entity_form', 'string');

        $resolver->setRequired('discriminator_class');
        $resolver->setAllowedTypes('discriminator_class', 'string');
        $resolver->addNormalizer('attr', function (OptionsResolver $resolver, array $attr): array {
            $attr['data-discriminator'] = 'true';

            return $attr;
        });
    }

    private function getOnLoadScript(): string
    {
        // discriminator select box is not changeable anymore on errors
        $script = <<<JS
if (this.value) {
    this.disabled = true;
    this.closest('div[data-discriminator]')
        .querySelector('div[data-discriminator-name='+this.value+']')
        .style.display = 'block';
}
JS;

        return 'javascript: ' . trim(preg_replace('/\s+/', ' ', $script));
    }

    private function getOnChangeScript(): string
    {
        $script = <<<JS
container = this.closest('div[data-discriminator]');
var elements = container.querySelectorAll('div[data-discriminator-name]');
for (const element of elements) {
    selected = this.value === element.getAttribute('data-discriminator-name');
    element.style.display = selected ? 'block' : 'none';
    if (selected) {
        for (const input of element.querySelectorAll('[data-required]')) {
            input.removeAttribute('data-required');
            input.setAttribute('required', 'true');
        }
    } else {
        for (const input of element.querySelectorAll('[required]')) {
            input.removeAttribute('required');
            input.setAttribute('data-required', 'true');
        }
    }
}
JS;

        return 'javascript: ' . trim(preg_replace('/\s+/', ' ', $script));
    }

    private function getShortName(string $className): string
    {
        $parts = explode('\\', $className);

        return end($parts);
    }
}
