<?php

namespace GetRepo\FormYaml\Type;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/** @see https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-text-security */
class ProtectedTextareaType extends ProtectedTextType
{
    public function getParent(): ?string
    {
        return TextareaType::class;
    }
}
