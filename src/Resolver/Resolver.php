<?php

namespace GetRepo\FormYaml\Resolver;

use Closure;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use GetRepo\FormYaml\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\String\CodePointString;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Composite;

class Resolver
{
    /** @var string[]  */
    private array $defaultTypeNamespaces = [
        'Symfony\Component\Form\Extension\Core\Type\\',
        'Symfony\Bridge\Doctrine\Form\Type\\',
        'GetRepo\FormYaml\Type\\',
    ];

    /** @var string[]  */
    private array $extraTypeNamespaces = [];

    /** @var string[]  */
    private array $defaultConstraintNamespaces = [
        'Symfony\Component\Validator\Constraints\\',
        'Symfony\Bridge\Doctrine\Validator\Constraints\\',
    ];

    public function __construct(
        private ?ContainerInterface $container = null,
    ) {
    }

    public function addTypeNamespaces(array $typeNamespaces): self
    {
        $this->extraTypeNamespaces = array_merge($typeNamespaces, $this->extraTypeNamespaces);

        return $this;
    }

    public function addConstraintNamespaces(array $constraintNamespaces): self
    {
        $this->defaultConstraintNamespaces = array_merge($constraintNamespaces, $this->defaultConstraintNamespaces);

        return $this;
    }

    public function isFormClass(string $formClass): bool
    {
        return class_exists($formClass) && isset(class_implements($formClass)[FormTypeInterface::class]);
    }

    public function resolveFormClass(string $formClass, array $options = []): ?string
    {
        $resolver = new OptionsResolver();
        $resolver->setDefault('default', null);
        $resolver->setAllowedTypes('default', ['null', 'string']);
        $resolver->setDefault('suffix', null);
        $resolver->setAllowedTypes('suffix', ['null', 'string']);
        $resolver->setDefault('disable_symfony_namespace', false);
        $resolver->setAllowedTypes('disable_symfony_namespace', 'bool');
        $options = $resolver->resolve($options);

        $namespaces = $this->extraTypeNamespaces;
        if (!$options['disable_symfony_namespace']) {
            $namespaces = array_merge($namespaces, $this->defaultTypeNamespaces);
        }

        if (!$this->isFormClass($formClass)) {
            $string = (new CodePointString($formClass))->camel()->title()->trim();
            foreach ($namespaces as $namespace) {
                if (
                    $options['disable_symfony_namespace']
                    && str_starts_with($namespace, 'Symfony\\')
                ) {
                    continue;
                }
                $stringNamespace = $string->prepend($namespace);
                $guesses = \array_unique([
                    $stringNamespace,
                    $stringNamespace->append('Type'),
                ]);
                if ($options['suffix']) {
                    $guesses[] = $stringNamespace->append($options['suffix']);
                    $guesses[] = $stringNamespace->append($options['suffix'])->append('Type');
                }
                foreach ($guesses as $guess) {
                    if ($this->isFormClass($guess)) {
                        return $guess;
                    }
                }
            }

            if (!$options['default']) {
                throw new RuntimeException(sprintf('Form class "%s" could not be resolved.', $formClass));
            }
            $formClass = $options['default'];
        }

        return $formClass;
    }

    public function resolveFormOptions(string $formClass, array $options): array
    {
        $formClass = $this->resolveFormClass($formClass);

        // resolve constraints
        if ($options['constraints'] ?? false) {
            $options['constraints'] = $this->resolveConstraints($options['constraints']);
        }

        // resolve entry_type
        if ($options['entry_type'] ?? false) {
            $options['entry_type'] = $this->resolveFormClass($options['entry_type']);
        }

        // resolve query_builder as string
        if (is_array($options['query_builder'] ?? false)) {
            $options['query_builder'] = $this->resolveQueryBuilder($options['query_builder'], $options);
        }

        switch ($formClass) {
            case CheckboxType::class:
                if (!array_key_exists('required', $options)) {
                    $options['required'] = false;
                }
                break;
            case ChoiceType::class:
                if ($options['choice_loader'] ?? false) {
                    $options['choice_loader'] = new CallbackChoiceLoader(
                        $this->resolveCallBack($options['choice_loader']),
                    );
                }
                if ($options['choice_attr'] ?? false) {
                    try {
                        $options['choice_attr'] = $this->resolveCallBack($options['choice_attr']);
                    } catch (RuntimeException $e) {
                        // choice attr will have its initial value
                    }
                }
                break;
            case RepeatedType::class:
                $options['type'] = $this->resolveFormClass($options['type']);
                break;
            case CollectionType::class:
                if ($options['entry_options'] ?? false) {
                    $options['entry_options'] = $this->resolveFormOptions($formClass, $options['entry_options']);
                }
                break;
        }

        return $options;
    }

    public function isConstraintClass(string $constraintClass): bool
    {
        return class_exists($constraintClass) && isset(class_parents($constraintClass)[Constraint::class]);
    }

    public function resolveConstraintClass(string $constraintClass, bool $throw = true): ?string
    {
        if (!$this->isConstraintClass($constraintClass)) {
            $string = (new CodePointString($constraintClass))->camel()->title()->trim();
            foreach ($this->defaultConstraintNamespaces as $namespace) {
                $stringNamespace = $string->prepend($namespace);
                $guesses = \array_unique([
                    $stringNamespace,
                ]);
                foreach ($guesses as $guess) {
                    if ($this->isConstraintClass($guess)) {
                        return $guess;
                    }
                }
            }

            if ($throw) {
                throw new RuntimeException(sprintf('Constraint class "%s" could not be resolved.', $constraintClass));
            }
            $constraintClass = null;
        }

        return $constraintClass;
    }

    public function resolveConstraints(mixed $constraints): array
    {
        $resolved = [];
        if (is_string($constraints)) {
            $resolved = [$this->resolveConstraint($constraints)];
        } elseif (
            is_array($constraints)
            && ['object'] !== array_unique(array_map('gettype', $constraints))
        ) {
            $resolved = [];
            foreach ($constraints as $constraintClass => $constraintArgs) {
                if (is_int($constraintClass)) {
                    $constraintArgs = (array) $constraintArgs;
                    /** @var string $constraintClass */
                    $constraintClass = key($constraintArgs);
                    $constraintArgs = current($constraintArgs);
                }
                if ($this->isComposite($constraintClass)) {
                    if (isset($constraintArgs['constraints'])) {
                        $constraintArgs['constraints'] = $this->resolveConstraints($constraintArgs['constraints']);
                    } else {
                        $constraintArgs = ['constraints' => $this->resolveConstraints($constraintArgs)];
                    }
                }
                $resolved[] = $this->resolveConstraint((string) $constraintClass, (array) $constraintArgs);
            }
        } else {
            $resolved = (array) $constraints;
        }

        return $resolved;
    }

    public function resolveConstraint(string $constraintClass, array $args = []): Constraint
    {
        $constraintClass = $this->resolveConstraintClass($constraintClass);

        // specific constraints options
        switch ($constraintClass) {
            case Choice::class:
                // optional callback
                if (isset($args['callback'])) {
                    $callback = $this->resolveCallBack($args['callback']);
                    $args['choices'] = $callback();
                    unset($args['callback']);
                }
                break;
        }

        // handle composite constraints
        if ($this->isComposite($constraintClass)) {
            if (isset($args['constraints'])) {
                $args['constraints'] = $this->resolveConstraints($args['constraints']);
            } else {
                $args = ['constraints' => $this->resolveConstraints($args)];
            }
        }

        /** @var Constraint $constraint */
        $constraint = new $constraintClass(...$args);

        return $constraint;
    }

    public function resolveCallBack(mixed $callback): Closure
    {
        if ($callback instanceof Closure) {
            return $callback;
        }

        if (is_array($callback)) {
            /** @var array $callback */
            $callback = array_values($callback);
        }

        // check already a valid callable
        if (\is_callable($callback)) {
            return Closure::fromCallable($callback);
        }

        if ($this->container && \is_array($callback) && 2 === count($callback)) {
            $callback = \array_values($callback);
            if (
                \is_string($callback[0])
                && '@' === $callback[0][0]
            ) {
                if (
                    $this->container->has($service = trim($callback[0], '@'))
                    && is_callable([$service = $this->container->get($service), $method = $callback[1]])
                ) {
                    // @phpstan-ignore-next-line
                    return Closure::fromCallable([$service, $method]);
                }
            }
        }

        if (is_object($callback[0] ?? null)) {
            $callback[0] = sprintf('Object(%s)', get_class($callback[0]));
        }

        throw new RuntimeException(sprintf(
            'Callback %s could not be resolved.',
            preg_replace('/(\s{2,}|\n)/', ' ', var_export($callback, true)),
        ));
    }

    public function resolveQueryBuilder(array $queryBuilder, array $options = []): Closure
    {
        // check first if queryBuilder is a callback
        try {
            return $this->resolveCallBack($queryBuilder);
        } catch (RuntimeException $e) {
            // nothing to do
        }

        // resolver QueryBuilder as array options
        $resolver = new OptionsResolver();
        // default alias
        $resolver->setDefault('alias', 'e');
        $resolver->setNormalizer('alias', function (OptionsResolver $resolver, $alias) use ($queryBuilder, $options) {
            if (
                !isset($queryBuilder['alias']) // if 'alias' is not defined by user
                && isset($options['class']) // if 'class' is set in form type options
                && is_string($options['class'])
                && class_exists($options['class'])
            ) {
                $parts = explode('\\', $options['class']);
                $alias = strtolower(end($parts)[0]);
            }

            return $alias;
        });
        $resolver->setAllowedTypes('alias', 'string');
        // defined methods map here
        $methodsMap = [
            // conditons
            'where' => ['allowed_type' => 'string'],
            'andWhere' => ['allowed_type' => 'string'],
            'orWhere' => ['allowed_type' => 'string'],
            'having' => ['allowed_type' => 'string'],
            // joins
            'join' => ['allowed_type' => 'array'],
            'innerJoin' => ['allowed_type' => 'array'],
            'leftJoin' => ['allowed_type' => 'array'],
            // havings
            'andHaving' => ['allowed_type' => 'string'],
            'orHaving' => ['allowed_type' => 'string'],
            // orders
            'orderBy' => ['allowed_type' => 'array'],
            'addOrderBy' => ['allowed_type' => 'array'],
        ];
        foreach ($methodsMap as $name => $map) {
            $resolver->setDefault($name, null);
            $resolver->setAllowedTypes($name, ['null', $map['allowed_type']]);
        }
        $queryBuilder = $resolver->resolve($queryBuilder);

        return function (EntityRepository $repo) use ($queryBuilder): QueryBuilder {
            $qb = $repo->createQueryBuilder($queryBuilder['alias']);
            // remove options that are not methods
            unset($queryBuilder['alias']);
            foreach ($queryBuilder as $method => $arg) {
                if (!is_null($arg)) {
                    if (is_array($arg)) {
                        $qb = call_user_func_array([$qb, $method], $arg); // @phpstan-ignore-line
                    } else {
                        $qb = call_user_func([$qb, $method], $arg); // @phpstan-ignore-line
                    }
                }
            }

            return $qb;
        };
    }

    private function isComposite(string $constraintClass): bool
    {
        return Composite::class === get_parent_class($this->resolveConstraintClass($constraintClass));
    }
}
