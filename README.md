<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/17571624/formyaml_logo.png" height=100 />
</p>

<h1 align=center>FormYaml</h1>
<h4 align=center style="border-bottom: 3px solid #008033; padding-bottom: 15px;">
    A Symfony bundle to setup forms and validation using YAML
</h4>

<br/>
Relying on [symfony/form](https://github.com/symfony/form), FormYaml
allows you to generate symfony forms in YAML. It gives you a few essential features to make it
very easy to generate forms with constraints in a readable and easy
to edit way, so that everyone on your team can tweak the forms if needed.

## Table of Contents

1. [Installation](#installation)
1. [Example](#example)
1. [Complete Reference](#complete-reference)
    1. [Enabled](#enabled)
    1. [Type](#type)
    1. [Condition](#condition)
    1. [Events](#events)
    1. [Options](#options)
        1. [constraints](#constraints-option)
        1. [entry_type](#entry_type-option)
        1. [query_builder](#query_builder-option)
1. [Resolver](#resolver)
1. [Custom Form Types](#custom-form-types)
    1. [DiscriminatorType](#discriminator-form-type)
    1. [JSCollectionType](#jscollection-form-type)
    1. [IconType](#icon-form-type)
    1. [FontAwesomeIconType](#fontawesomeicon-form-type)
    1. [ProtectedTextType](#protected-text-form-type)
    1. [ProtectedTextareaType](#protected-textarea-form-type)

<br/><br/>
## Installation

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/form-yaml git https://gitlab.com/get-repo/form-yaml.git
    composer require get-repo/form-yaml

<br/><br/>
## Example

Here is a complete example of form definitions in YAML :
```yaml
# /path/to/form.yml
form_definitions:
    field1:
        enabled: true # enabled or disable the field
        type: Choice # form type
        condition: 'action in ["new"]' # expression language condition to display the field (with values form_name, form_options)
        options: # form type options
            choices:
                Choice 1: 1
                Choice 2: 2
            required: true
            attr:
                placeholder: 'Please select a choice'
        events: # form type events (callbacks)
            pre_set_data:
                callback: [App\Entity\User, 'getChoices']
                priority: 10
            pre_set_data:
                callback: ['@service_name', 'methodName']
    field2:
        type: Input
    # ... etc
```

Here is an example how to use it in a standalone way :
```php

use Symfony\Component\Form\Forms;
use Symfony\Component\Validator\Validation;
use GetRepo\FormYaml\ConfigurableEntityType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;

$type = new ConfigurableEntityType();
$validator = Validation::createValidator();

$data = new class()
{
    public int $count = 1;
    public string $name = 'test';
};

$form = Forms::createFormFactoryBuilder()
    ->addType($type)
    ->addExtension(new ValidatorExtension($validator))
    ->getFormFactory()
    ->create(ConfigurableEntityType::class, $data, ['form_definitions' => [
        'count' => ['type' => 'Number'],
        'name' => ['type' => 'Text'],
    ]]);

$form->all(); // get all form fields

```

<br/><br/>

## Complete Reference

### Enabled
The **enabled** boolean defines if with we use that field. That's it !

<br/>

### Type
The **type** defines the symfony form type to use.
https://symfony.com/doc/current/reference/forms/types.html

There is an auto-discover system to avoid writing the full class. It looks in the following namespaces:
 - `Symfony\Component\Form\Extension\Core\Type`
 - `Symfony\Bridge\Doctrine\Form\Type`
 - `GetRepo\FormYaml\Type`

Some examples below how you can define some types:
```yaml
# full namespace class
type: Symfony\Component\Form\Extension\Core\Type\EmailType
# class name
type: EmailType
# name
type: email
```

You can add more auto-discovered form type namespaces using the `GetRepo\FormYaml\Resolver\Resolver` class :
```php
$resolver->addTypeNamespaces('Custom\Type');
```

<br/>

### Condition
The **condition** defines if the field should be displayed or not.
It should be an [expression language syntax](https://symfony.com/doc/current/components/expression_language/syntax.html) string,
with the following values:
 - `form_name` Name of the form
 - `form_options` Options of the form
 
Some examples below how you can define some conditions:
```yaml
condition: 'form_name == "test"'
condition: 'form_options["required"] == false'
```

You can use the `expression_language_values` form option to add custom expression language values.

<br/>

### Events
The **events** defines [Symfony form events](https://symfony.com/doc/current/form/events.html) in a YAML way, using callbacks.
```yaml
# callback examples :
callback: [App\Entity\User, 'getChoices'] # static callback
callback: ['@my_service, 'getChoices'] # service container callback
```

<br/>

### Options
The **options** defines the form fields options (from the symfony form type).

#### <u>constraints option</u>

The **constraints** option defines validation constraints of the form field.
https://symfony.com/doc/current/reference/constraints.html

There is an auto-discover system to avoid writing the full constraint definitions. It looks in the following namespaces:

 - `Symfony\Component\Validator\Constraints`
 - `Symfony\Bridge\Doctrine\Validator\Constraints`

Some examples below how you can define some types:
```yaml
options:
    constraints:
        - NotBlank: ~
# Choice callback
options:
    constraints:
        - Choice:
            callback: ['@service_name', 'methodName']
```

You can add more auto-discovered constraint namespaces using the `GetRepo\FormYaml\Resolver\Resolver` class :
```php
$resolver->addConstraintNamespaces('Custom\Constraints');
```

#### <u>entry_type option</u>

*For CollectionType Field only*

The **entry_type** option defines the field type for each item in this collection (e.g. TextType, ChoiceType, etc).
For example, if you have an array of email addresses, you’d use the EmailType.
If you want to embed a collection of some other form, pass the form type class as this option (e.g. MyFormType::class).
https://symfony.com/doc/current/reference/forms/types/collection.html#entry-type

There is an auto-discover system like in [type](#type)

Some examples below how you can define some entry_type:
```yaml
# full namespace class
options:
    entry_type: Symfony\Component\Form\Extension\Core\Type\EmailType
# class name
options:
    entry_type: EmailType
# name
options:
    entry_type: email
```

#### <u>query_builder option</u>

The **query_builder** option defines a QueryBuilder in yaml. Only simple queries can be created at the moment.

Some examples below how you can define some query_builder:
```yaml
# full namespace class
options:
    query_builder:
        alias: 'a'
        join: ['b', 'a.id = 'b.id']  # also join innerJoin leftJoin
        where: "a.field == 'test'"   # also andWhere orWhere
        having: 'a.id > 100'         # also andHaving orHaving
        orderBy: ['a.id', 'ASC']     # also addOrderBy
```

<br/><br/>

## Resolver
The `GetRepo\FormYaml\Resolver\Resolver` resolver class tries to transform YAML syntax to form options, constraints objects, doctrine queries, and callbacks.

<br/><br/>

## Custom Form Types

Specific symfony form fields, with pure javascript in it.
<br/>

### Discriminator Form Type

Display of select box with the available class in the discriminator of an entity having single table inheritance.
https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/reference/inheritance-mapping.html#single-table-inheritance

```yaml
book:
    targetEntity: App\Entity\Book
    inversedBy: writer
    form:
        type: Discriminator
        options:
            discriminator_class: App\Entity\Book
```

<br/>

### JSCollection Form Type

This field type is used to render a collection managed in javascript of some field or form.
```yaml
books:
    targetEntity: App\Entity\Book
    mappedBy: writer
    cascade: [all]
    orphanRemoval: true
    form:
        type: SfyJSCollectionType
        options:
            allow_add: true
            allow_delete: true
            icon_add_class: 'fas fa-plus-circle text-success'
            icon_add_text: null
            icon_delete_class: 'fas fa-minus-circle text-danger'
            icon_delete_text: null
            container_class: 'row mx-1 px-3 border border-light rounded'
            mapped_path: template # force to set the template to each elements of the collection
            entry_options:
                data: App\Entity\Book
                submit: false
                row_attr:
                    class: p-0 m-3
                attr:
                    class: shadow p-3 bg-white rounded
            constraints:
                - Valid: ~
```

<br/>

### Icon Form Type

This field type is used to render a smiple text field that refer to an image URL, and will show the preview.
```yaml
...
    form:
        type: Icon
```

<br/>

### FontAwesomeIcon Form Type

This field type is used to render a smiple text field that refer to a [v5 font awesome icon](https://fontawesome.com/v5/search)
```yaml
...
    form:
        type: FontAwesomeIcon
        options:
            attr: [...] # field attributes
            help_html: true # show help html
            help: 'html ...' # html help content
            constraints: [...] # Form constraints
```

<br/>

### Protected Text Form Type

This field type is used to obfuscate the value (a bit like a password field) using the [webkit-text-security](https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-text-security) style.

```yaml
...
    form:
        type: ProtectedTextType
        options:
            security_style: 'circle' # circle, disc, square
```

<br/>

### Protected Textarea Form Type

Same [as above](#protected-text-form-type) with a textarea field.

```yaml
...
    form:
        type: ProtectedTextareaType
        options:
            security_style: 'circle' # circle, disc, square
```
