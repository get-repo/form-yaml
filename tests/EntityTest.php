<?php

namespace Test;

function dummy(): void
{
    // for callback test
}

class EntityTest
{
    private readonly int $id; // @phpstan-ignore-line
    private bool $enabled = true;
    private string $name;
    private string $title;

    /** @return int[] */
    public static function getChoices(): array
    {
        return [1, 2, 3];
    }

    public static function getAttr(mixed $value): array
    {
        return ['class' => $value];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function transformDataTitle(?string $title): string
    {
        return $title . ' - ' . __METHOD__;
    }

    public function reverseTransformDataTitle(?string $title): string
    {
        return $title . ' - ' . __METHOD__;
    }
}
