<?php

namespace Test;

use GetRepo\FormYaml\Type\IconType;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Forms;

class IconTypeTest extends TestCase
{
    public function testIsFormClass(): void
    {
        $form = Forms::createFormFactoryBuilder()
            ->addType(new IconType())
            ->getFormFactory()
            ->create(IconType::class);

        $vars = $form->createView()->vars;
        $this->assertIsArray($vars);
        $this->assertArrayHasKey('attr', $vars);
        $attr = $vars['attr'];
        $this->assertNotEmpty($attr);
        $this->assertArrayHasKey('onkeyup', $attr);
        $this->assertArrayHasKey('onload', $attr);

        $js = [
            "javascript: [].forEach.call(document.querySelectorAll('img.icon-icon'), function(el) {  el.remove();});",
            "label = document.querySelector('label[for=\"icon\"]')",
            "if (label) {  label.insertAdjacentHTML(  'afterend'",
        ];
        foreach ($js as $part) {
            $this->assertStringContainsString($part, $attr['onkeyup']);
            $this->assertStringContainsString($part, $attr['onload']);
        }
    }
}
