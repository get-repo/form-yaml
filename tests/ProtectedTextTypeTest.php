<?php

namespace Test;

use GetRepo\FormYaml\Type\ProtectedTextType;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Forms;

class ProtectedTextTypeTest extends TestCase
{
    protected static function typeClass(): string
    {
        return ProtectedTextType::class;
    }

    public function provider(): array
    {
        return [
            'invalid option security_style' => [
                '', // expects exception
                ['security_style' => 'invalid_style'],
                'The option "security_style" with value "invalid_style" is invalid.',
            ],
            'no options, default behaviour' => [
                'text-security: disc; -webkit-text-security: disc; -moz-text-security: disc;',
            ],
            'option security_style circle' => [
                'text-security: circle; -webkit-text-security: circle; -moz-text-security: circle;',
                ['security_style' => 'circle'],
            ],
            'option "security_style" with existing style ending with ;' => [
                'color: red; text-security: square; -webkit-text-security: square; -moz-text-security: square;',
                ['attr' => ['style' => 'color: red;'], 'security_style' => 'square'],
            ],
            'option "security_style" with existing style ending with many ";" everywhere' => [
                'color: green; text-security: disc; -webkit-text-security: disc; -moz-text-security: disc;',
                ['attr' => ['style' => ';;color: green;;;  ']],
            ],
            'option "security_style" with existing style not ending with ;' => [
                'color: blue; text-security: square; -webkit-text-security: square; -moz-text-security: square;',
                ['attr' => ['style' => 'color: blue'], 'security_style' => 'square'],
            ],
        ];
    }

    /**
     * @dataProvider provider
     */
    public function test(string $expectedStyle, array $options = [], string $expectedExpection = null): void
    {
        $testedTypeClass = static::typeClass();
        if ($expectedExpection) {
            $this->expectExceptionMessage($expectedExpection);
        }
        $form = Forms::createFormFactoryBuilder()
            ->addType(new $testedTypeClass()) // @phpstan-ignore-line
            ->getFormFactory()
            ->create($testedTypeClass, null, $options);

        $vars = $form->createView()->vars;
        $this->assertArrayHasKey('attr', $vars);
        $attr = $vars['attr'];
        $this->assertNotEmpty($attr);
        $this->assertArrayHasKey('style', $attr);
        $this->assertEquals($expectedStyle, $attr['style']);
    }
}
