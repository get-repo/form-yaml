<?php

namespace Test;

use GetRepo\FormYaml\Type\FontAwesomeIconType;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Forms;

class FontAwesomeIconTypeTest extends TestCase
{
    public function testIsFormClass(): void
    {
        $form = Forms::createFormFactoryBuilder()
            ->addType(new FontAwesomeIconType())
            ->getFormFactory()
            ->create(FontAwesomeIconType::class);

        $vars = $form->createView()->vars;
        $this->assertIsArray($vars);
        $this->assertArrayHasKey('attr', $vars);
        $attr = $vars['attr'];
        $this->assertNotEmpty($attr);
        $this->assertArrayHasKey('placeholder', $attr);
        $this->assertArrayHasKey('onkeyup', $attr);
        $this->assertArrayHasKey('onload', $attr);
        $this->assertEquals('Font Awesome icon class (ex: fas fa-won-sign)', $attr['placeholder']);
        $js = "pEl = this.parentNode.querySelector(\".preview\");pEl.innerHTML = '<i class=\"'+this.value+'\"></i>';";
        $this->assertStringEndsWith($js, $attr['onkeyup']);
        $this->assertStringEndsWith($js, $attr['onload']);
    }
}
